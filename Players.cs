using System;
using System.Collections.Generic;

namespace Uno2P
{
    class Players
    {
        public List<Hand> playerlist;

        public Players(int numplayers, Deck deckname)
        {
            playerlist = new List<Hand>();
            for(int playercount = 0; playercount < numplayers; playercount ++)
            {
                Console.WriteLine("What is the player name?");
                string nameplayer = Console.ReadLine();
                playerlist.Add(new Hand(deckname, nameplayer));
            }
        }
    }
}