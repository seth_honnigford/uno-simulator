using System;
using System.Collections.Generic;
namespace Uno2P
{
    class Program
    {
        static void Main(string[] args)
        {
            Deck myDeck = new Deck();
            myDeck.Shuffle();
            Discard myDiscard = new Discard(myDeck);
            Console.WriteLine("How many players?");
            int playercount = Convert.ToInt32(Console.ReadLine());
            Players myPlayers = new Players(playercount, myDeck);
            int turncount = 0;
            int stepvalue = 1;
            for(int n = 0; n < 1;)
            {
                Console.Clear();
                bool drawn = false;
                int turnorder = turncount % playercount;
                Console.WriteLine("It is " + myPlayers.playerlist[turnorder].playername + "'s turn, press enter to continue");
                Console.ReadLine();
                while(drawn == false)
                {
                Console.Clear();
                for(int listcount = 0; listcount < myPlayers.playerlist[turnorder].handlist.Count; listcount ++)
                {
                    if(myPlayers.playerlist[turnorder].handlist[listcount].id == "Wild" || myPlayers.playerlist[turnorder].handlist[listcount].id == "Draw 4")
                    {
                        Console.WriteLine((listcount + 1) + "." + myPlayers.playerlist[turnorder].handlist[listcount].id);
                    }
                    else
                    {
                        Console.WriteLine((listcount + 1) + "." + myPlayers.playerlist[turnorder].handlist[listcount].color + " " + myPlayers.playerlist[turnorder].handlist[listcount].id);
                    }
                }
                Console.WriteLine((myPlayers.playerlist[turnorder].handlist.Count + 1) + ".Draw");
                Console.WriteLine("Card on top of the discard is: " + myDiscard.discardlist[myDiscard.discardlist.Count - 1].color + " "+ myDiscard.discardlist[myDiscard.discardlist.Count - 1].id);
                Console.Write("What card do you want to play? ");
                int cardplay = Convert.ToInt32(Console.ReadLine());
                if(cardplay == myPlayers.playerlist[turnorder].handlist.Count + 1)
                {
                    myPlayers.playerlist[turnorder].DrawCard(myDeck, 1, myDiscard);
                    Console.WriteLine("The card you draw is " + myPlayers.playerlist[turnorder].handlist[myPlayers.playerlist[turnorder].handlist.Count - 1].color + " " + myPlayers.playerlist[turnorder].handlist[myPlayers.playerlist[turnorder].handlist.Count - 1].id);
                    Console.WriteLine("Press Enter to continue");
                    Console.ReadLine();
                    drawn = true;
                }
                if(myPlayers.playerlist[turnorder].handlist[cardplay - 1].color == myDiscard.discardlist[myDiscard.discardlist.Count - 1].color
                || myPlayers.playerlist[turnorder].handlist[cardplay - 1].id == myDiscard.discardlist[myDiscard.discardlist.Count - 1].id
                || myPlayers.playerlist[turnorder].handlist[cardplay - 1].id == "Wild"
                || myPlayers.playerlist[turnorder].handlist[cardplay - 1].id == "Draw 4")
                {
                    switch(myPlayers.playerlist[turnorder].handlist[cardplay - 1].id)
                    {
                        case "Draw 2":
                            myPlayers.playerlist[(turnorder + stepvalue) % playercount].DrawCard(myDeck, 2, myDiscard);
                            turncount = turncount + stepvalue;
                            break;
                        case "Reverse":
                            stepvalue = stepvalue * -1;
                            break;
                        case "Skip":
                            turncount = turncount + stepvalue;
                            break;
                        case "Wild":
                            Console.WriteLine("Choose a color");
                            myPlayers.playerlist[turnorder].handlist[cardplay - 1].color = Console.ReadLine();
                            break;
                        case "Draw 4":
                            myPlayers.playerlist[(turnorder + stepvalue) % playercount].DrawCard(myDeck, 4, myDiscard);
                            turncount = turncount + stepvalue;
                            Console.WriteLine("Choose a color");
                            myPlayers.playerlist[turnorder].handlist[cardplay - 1].color = Console.ReadLine();
                            break;
                }
                myDiscard.discardlist.Add(myPlayers.playerlist[turnorder].handlist[cardplay - 1]);
                myPlayers.playerlist[turnorder].handlist.Remove(myPlayers.playerlist[turnorder].handlist[cardplay - 1]);
                break;
                }
                }
                turncount = turncount + stepvalue;
                if(myPlayers.playerlist[turnorder].handlist.Count == 0)
                {
                    Console.Clear();
                    Console.WriteLine(myPlayers.playerlist[turnorder].playername + " Wins!");
                    goto End;
                }
            }
            End:
            Console.WriteLine("Done");
        }
    }
}