using System;
using System.Collections.Generic;

namespace Uno2P
{
    class Card
    {
        public string color;
        public string id;
        public Card(string Color, string ID)
        {
            color = Color;
            id = ID;
        }
    }
}