using System;
using System.Collections.Generic;

namespace Uno2P
{
    class Hand
    {
        public List<Card> handlist;
        public string playername;
        public Hand(Deck deckname, string Playername)
        {
            playername = Playername;
            handlist = new List<Card>();
            while(handlist.Count < 7)
            {
                handlist.Add(deckname.decklist[0]);
                deckname.decklist.Remove(deckname.decklist[0]);
            }
        }

        public void DrawCard(Deck deckname, int drawnum, Discard discardname)
        {
            for(int drawcount = 0; drawcount < drawnum; drawcount ++)
            {
                handlist.Add(deckname.decklist[0]);
                deckname.decklist.Remove(deckname.decklist[0]);
                if(deckname.decklist.Count == 0)
                {
                    discardname.Reshuffle(deckname);
                }
            }
        }
    }
}