using System;
using System.Collections.Generic;

namespace Uno2P
{
    class Deck
    {
       public List<Card> decklist;
       public Deck()
       {
           decklist = new List<Card>();
           string[] colors = {"Red", "Blue", "Yellow", "Green"};
            string[] idnums = {"0", "1", "1", "2", "2", "3", "3", "4", "4", "5", "5", "6", "6", "7", "7", "8", "8",
            "9", "9", "Draw 2", "Draw 2", "Reverse", "Reverse", "Skip", "Skip", "Wild", "Draw 4"};
            int cardnum = 0;
           for(int idnum = 0; idnum < 27; idnum ++)
           {
               for(int colorpicker = 0; colorpicker < 4; colorpicker ++)
               {
                   decklist.Add(new Card(colors[colorpicker], idnums[idnum]));
                   cardnum ++;
               }
           }
       } 
        public void Shuffle()
        {
            Random r = new Random();
            for(int shufflecount = 0; shufflecount < 108; shufflecount ++)
            {
                Card temp = decklist[shufflecount];
                int shufflenum = r.Next(shufflecount,decklist.Count);
                decklist[shufflecount] = decklist[shufflenum];
                decklist[shufflenum] = temp;
            }
        }
    }
}