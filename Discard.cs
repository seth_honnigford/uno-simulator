using System;
using System.Collections.Generic;

namespace Uno2P
{
    class Discard
    {
        public List<Card> discardlist;
        public Discard(Deck deckname)
        {
            discardlist = new List<Card>();
            discardlist.Add(deckname.decklist[0]);
            deckname.decklist.Remove(deckname.decklist[0]);
        }
        public void Reshuffle(Deck deckname)
        {
            while(discardlist.Count > 1)
            {
                deckname.decklist.Add(discardlist[0]);
                discardlist.Remove(discardlist[0]);
                deckname.Shuffle();
            }
        }
    }
}